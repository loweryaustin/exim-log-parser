<!DOCTYPE html>
<html>
<head>
	<title>
	Exim Log Parser
	</title>
	<?php require('elements/links.php') ?>
</head>
<body>

	<div id="main-container" class="container_16">
		<h1> Enter your logs below.</h1>
		<div class="grid_16">
			<nav id="main-navigation">
				<ul>
					<a href="/"><li>Home</li></a>
					<a href="telpa"><li>TELPa</li></a>
					<a href="kb"><li>KnowledgeBase</li></a>
				</ul>
			</nav>
		</div>
		<div class="grid_16">
			<form action="telpa/parse" method="post">
				<textarea name="zeh_logs" rows="20" cols="130"></textarea>
				<br />
				<br />
				<input type="submit" name="submit" value="Parse!" />
				<br />
			<!-- 	<label>Debug?</label>
				<input type="checkbox"name="debug" value="TRUE" /> -->
			</form>
		</div>
		
	</div>
</body>
</html>
