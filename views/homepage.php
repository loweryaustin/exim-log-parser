<!DOCTYPE html>
<html>
<head>
	<title>Exim Tricks, Making Mail Easier</title>
	<?php require('elements/links.php') ?>
</head>
<body>

	<div id="main-container" class="container_16">
		<div id="header" class="grid_16">
			<h1>Exim Tricks</h1>
			<h2>Making Mail Easier</h2>
		</div>
		<div class="grid_16">
			<nav id="main-navigation">
				<ul>
					<a href="/"><li>Home</li></a>
					<a href="telpa"><li>TELPa</li></a>
					<a href="kb"><li>KnowledgeBase</li></a>
				</ul>
			</nav>
		</div>
		<div class="grid_14 push_1">
			
			<h2><span class="blue">TELPa: T</span>he <span class="blue">E</span>xim <span class="blue">L</span>og <span class="blue">Pa</span>rser 0.1.0Beta has been released!</h2>
			<p>
				TELPa is a little app that I created mainly so that I could practice understanding exim logs as well as my PHP programming skills. I think it will also help speed up the examination of exim logs, so that you can spend less time going through them, and get on to fixing your mail woes.
			</p>
			<p>
				At the current time, TELPa is very lacking in features, and still has a lot to learn about how to read the logs. It is focused mainly around individual message transactions, but I have plans to add more functionality. For a while you will probably see entries that say to consult the logs or entries that are vauge or just simply incorrect. The one thing that it does really well right now, is grouping all of the relevant logs for a message transaction in one place, so you can enter a large block of logs and get them sorted into blocks of message transactions at the very least. Soon I will be implementing a nubmer of different features, and at the top of that list is the ability to filter the message transactions by email address, time and date, etc, so that you can sort through a very large block of exim logs, or even upload entire log files and get the information that you need quickly. 
			</p>
			<p>
				I have plans to publish TELPa as an open source application, but at this time I am still learning about that process. Sure I could probably just post the source and be done with it, but I like to fully research things and do them right. I may wait till version 1.0.0 to do that anyway.
			</p>
		</div>
	</div>

</body>
</html>