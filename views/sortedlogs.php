<!DOCTYPE html>
<html>
<head>
	<title>Enter your logs - TELPa The Exim Log Parser</title>
	<?php require('elements/links.php') ?>
</head>
<body>
	<div id="main-container" class="container_16">
		<div id="header" class="grid_16">
			<h1>Exim Tricks</h1>
			<h2>Making Mail Easier</h2>
		</div>
		<div class="grid_16">
			<nav id="main-navigation">
				<ul>
					<li><a href="/">Home</a></li>
					<li><a href="telpa">TELPa</a></li>
					<li><a href="kb">KnowledgeBase</a></li>
				</ul>
			</nav>
		</div>
		<div class="grid_16">

			<label for="autocomplete">Filter By Email Address(broken ATM): </label>
			<input id="autocomplete">
			<script>
			var tags = [ "c++", "java", "php", "coldfusion", "javascript", "asp", "ruby" ];
			$( "#autocomplete" ).autocomplete({
			source: function( request, response ) {
			var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
			response( $.grep( tags, function( item ){
			return matcher.test( item );
			}) );
			}
			});
			</script>

			<?php foreach ($data as $message_id => $message_array): ?>
				<div class="tabs">
					<ul>
					<li><a href="#fragment-1"><span>Message Details</span></a></li>
					<li><a href="#fragment-2"><span>Raw Logs</span></a></li>
					<li><a href="#fragment-3"><span>Statistics</span></a></li>
					</ul>
					<div id="fragment-1">
						<span style="display:block">Origin Address: <?php if (isset($message_array['message_attrs']['from_address'])) {
							echo $message_array['message_attrs']['from_address'];
						}else{
							echo "Consult Raw Logs";
						} ?></span>
						<span style='display:block'>
						Origin Host: <?php 
							if (isset($message_array['message_attrs']['origin_host'])) {
								echo $message_array['message_attrs']['origin_host'];
							}else{
								echo "Consult Raw Logs";
							}
						 ?>
						</span>
						<span style='display:block'>
						<?php 
							if (isset($message_array['message_attrs']['user'])) {
								echo 'Sending User:'.$message_array['message_attrs']['user'];
							}
						 ?>
						</span>
						<br />
						<span style='display:block;'>
							Recipient Address: <?php 
							if (isset($message_array['message_attrs']['to_address'])) {
								echo $message_array['message_attrs']['to_address'];
							}else{
								echo "Consult Raw Logs";
							}
							 ?>
						</span>
						<span style='display:block;'>
							Recipient Host: <?php 
							if (isset($message_array['message_attrs']['recipient_host'])) {
								echo $message_array['message_attrs']['recipient_host'];
							}else{
								echo "Consult Raw Logs";
							}
							 ?>
						</span>
						<br />
						<span style='display:block;'>
							Protocol Used: <?php 
							if (isset($message_array['message_attrs']['protocol'])) {
								echo $message_array['message_attrs']['protocol'];
							}else{
								echo "Consult Raw Logs";
							}
							 ?>
						</span>
					</div>
					<div id="fragment-2">
						<?php foreach ($message_array as $message_array_key => $log_array) {
							if ($message_array_key !== 'message_attrs') {
								echo '<span>'.$log_array['raw_log'].'</span><br /><br />';
							}
								
							
						} ?>
					</div>
					<div id="fragment-3">
						Statistics have not been coded yet :'{
					</div>
				</div>
			<?php endforeach ?>

			
				<script>
				$( ".tabs" ).tabs();
				</script>
		</div>
	</div>
</body>
</html>
