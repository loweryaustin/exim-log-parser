<?php

    //The array we begin with
    $start_array = array('foo' => 'bar', 'bar' => 'foo', 'foobar' => 'barfoo');
    
    //Convert the array to a string
    $array_string = <<<ARRAY
Array (
[0] => Array
    (
    )

[1] => Array
    (
        [0] => Array
            (
                [id] => 74
                [RecordGUID] => 9BD28E1E-99EB-D4E7-CC2C-AB6F5905BCDA
                [Type] => DENTAL
                [App_Service] => a:4:{i:151;s:6:"AAMCAS";i:152;s:3:"DDS";i:154;s:11:"APP SVC TWO";i:155;s:6:"AADSAS";}
            )

        [1] => Array
            (
                [id] => 73
                [RecordGUID] => A5146CFF-5D17-FB1A-D831-E6835D0A04DD
                [Type] => MED
                [App_Service] => a:1:{i:151;s:6:"AAMCAS";}
            )

        [2] => Array
            (
                [id] => 75
                [RecordGUID] => 0C253109-07E7-151A-0277-19EAC025C2E6
                [Type] => PHYSICAL THERAPY
                [App_Service] => a:1:{i:153;s:8:"PHYSTHER";}
            )

    )

[2] => Array
    (
        [0] => Array
            (
                [id] => 155
                [RecordGUID] => 5DF76F3E-2F0C-FD63-B58F-027A61E9BC11
                [AppService] => AADSAS
                [AppService_types] => 
            )

        [1] => Array
            (
                [id] => 151
                [RecordGUID] => 3B503CFC-AB80-C06B-C4C4-8EE548FFC7BF
                [AppService] => AAMCAS
                [AppService_types] => 
            )

        [2] => Array
            (
                [id] => 154
                [RecordGUID] => 753D95F2-6733-AE27-8F2E-48685DC796C0
                [AppService] => APP SVC TWO
                [AppService_types] => 
            )

        [3] => Array
            (
                [id] => 152
                [RecordGUID] => 0D3C9435-64DD-9079-C0F4-D543DFFA0E10
                [AppService] => DDS
                [AppService_types] => 
            )

        [4] => Array
            (
                [id] => 153
                [RecordGUID] => 0D196967-21AF-ADDA-920E-F12938DACADB
                [AppService] => PHYSTHER
                [AppService_types] => 
            )
    )
)
ARRAY;

    //Get the new array
    $end_array = text_to_array($array_string);
    
    //Output the array!
    print_r($end_array);
    
    function text_to_array($str) {

        //Initialize arrays
        $keys = array();
        $values = array();
        $output = array();
        
        //Is it an array?
        if( substr($str, 0, 5) == 'Array' ) {
        
            //Let's parse it (hopefully it won't clash)
            $array_contents = substr($str, 7, -2);
            $array_contents = str_replace(array(' ', '[', ']', '=>'), array('', '#!#', '#?#', ''), $array_contents);
            $array_fields = explode("#!#", $array_contents);
            
            //For each array-field, we need to explode on the delimiters I've set and make it look funny.
            for($i = 0; $i < count($array_fields); $i++ ) {
            
                //First run is glitched, so let's pass on that one.
                if( $i != 0 ) {
                
                    $bits = explode('#?#', $array_fields[$i]);
                    if( $bits[0] != '' ) $output[$bits[0]] = $bits[1];
                
                }
            }
            
            //Return the output.
            return $output;
            
        } else {
            
            //Duh, not an array.
            echo 'The given parameter is not an array.';
            return null;
        }
        
    }