<?php

class ParseModel extends BaseModel{

	private $messageLogRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2}\s[0-9]{2}:[0-9]{2}:[0-9]{2}\s[A-Za-z0-9]{6}-[A-Za-z0-9]{6}-[A-Za-z0-9]{2}/"; //Regex that matches a log that is part of a message transction
	
	private $messageIdRegex = "/\b[A-Za-z0-9]{6}-[A-Za-z0-9]{6}-[A-Za-z0-9]{2}\b/";
	private $completedTransactionRegex = '/\b[A-Za-z0-9]{6}-[A-Za-z0-9]{6}-[A-Za-z0-9]{2}\sCompleted/';
	private $logLineFlagRegexes = array('/\s=>\s/','/\s<=\s/','/\s\*\*\s/','/\s\->\s/','/\s\*>\s/','/\s==\s/','/\b[A-Za-z0-9]{6}-[A-Za-z0-9]{6}-[A-Za-z0-9]{2}\sCompleted/');//private $logLineFlagRegexes;
	private $bounceMessageIdRegex = "/\bR=[A-Za-z0-9]{6}-[A-Za-z0-9]{6}-[A-Za-z0-9]{2}\b/";
	private $dateRegex = "/[0-9]{4}-[0-9]{2}-[0-9]{2}/";
	private $timeRegex = "/[0-9]{2}:[0-9]{2}:[0-9]{2}/";
	private $protocolRegex = "/P=[a-zA-z]*\b/"; //make sure to only match for protocol if the message is sending
	private $fromAddressRegex = "/<= [^>=+<@\n\r\s]+@[\w.-]+\.[A-Za-z]{2,4}/";
	private $toAddressRegex = "/=> \S+/";
	private $userRegex = "/\sU=[A-Za-z0-9_\-().\[\] ]+\s/"; // This could probably be improved. Should match a user.
	private $hostRegex = "/\sH=[A-Za-z0-9_\-().\[\] ]+\s\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\]:{0,1}[0-9]*/";
	private $testRegex = '/^2013/';

	public function getIndividualLogAttrs($raw_logs){
		$raw_logs = trim($raw_logs);
		$log_array = explode("\n", $raw_logs);
		$return_log_array = array();
		$new_log_key = 0;

		foreach ($log_array as $log_key => $individual_log) {
			if (preg_match($this->messageLogRegex, $individual_log, $date_time_id_string)) {
				$date_time_id_string = $date_time_id_string[0];

						foreach ($this->logLineFlagRegexes as $flag_key => $flagRegex) {

							if(preg_match($flagRegex, $individual_log, $flag_regex_result)){
								switch($flag_regex_result[0]){
									case ' => ':
										$return_log_array[$new_log_key]['type'] = 'delivery';
										break;
									case ' <= ':
										$return_log_array[$new_log_key]['type'] = 'arrival';

										break;
									case ' ** ':
										$return_log_array[$new_log_key]['type'] = 'failedDelivery';
										break;
									case ' -> ':
										$return_log_array[$new_log_key]['type'] = 'additionalDelivery';
										break;
									case ' *> ':
										$return_log_array[$new_log_key]['type'] = 'deliverySuppressed';
										break;
									case ' == ':
										$return_log_array[$new_log_key]['type'] = 'defferedDelivery';
										break;
									default:
										if (preg_match($this->completedTransactionRegex, $flag_regex_result[0])) {
											$return_log_array[$new_log_key]['type'] = "completion_log";
										}else{
											$return_log_array[$new_log_key]['type'] = 'unidentified_log_type'; // should never occurn due to the IF statement and the way this is structured. Will refactor later
										}
										break;
								}		
							}
						}

				preg_match($this->messageIdRegex, $date_time_id_string, $message_id);
					$return_log_array[$new_log_key]['message_id'] = $message_id[0];



				if (preg_match($this->dateRegex, $individual_log, $date)) {
					// $date_array = explode('-', $date[0]);
					// $date_array[0] = $date_array[0][3].$date_array[0][4].

					$return_log_array[$new_log_key]['date'] = $date[0];
				}





				if (preg_match($this->timeRegex, $individual_log, $time)) {
					$timeArray = explode(':', $time[0]);
					if ($timeArray[0] > 12) {
						$timeArray[3] = 'PM';
						$timeArray[0] = $timeArray[0] - 12;
					}else{
						if ($timeArray[0] == 12) {
							$timeArray[3] = 'PM';
						}else{
							$timeArray[3] = 'AM';
						}
					}

					$return_log_array[$new_log_key]['time'] = $timeArray[0].':'.$timeArray[1].':'.$timeArray[2].' '.$timeArray[3];
				}



				if (preg_match($this->protocolRegex, $individual_log, $protocol)) {
					$return_log_array[$new_log_key]['protocol'] = str_replace('P=', '', $protocol[0]);
				}else{
					//
				}



				if (preg_match($this->fromAddressRegex, $individual_log, $from_address)) {
					$return_log_array[$new_log_key]['from_address'] = str_replace('<= ', '', $from_address[0]);
				}else{
					unset($return_log_array[$new_log_key]['from_address']);
				}





				if (preg_match($this->hostRegex, $individual_log, $return_log_array[$new_log_key]['remote_server'])) {
					$return_log_array[$new_log_key]['remote_server'] = $return_log_array[$new_log_key]['remote_server'][0];
				}else{
					unset($return_log_array[$new_log_key]['remote_server']);
				}




				if (preg_match($this->userRegex, $individual_log, $return_log_array[$new_log_key]['user'])) {
					$return_log_array[$new_log_key]['user'] = str_replace(' U=', '', $return_log_array[$new_log_key]['user'][0]);
				}else{
					unset($return_log_array[$new_log_key]['user']);
				}

				if (preg_match($this->toAddressRegex, $individual_log, $return_log_array[$new_log_key]['to_address'])) {
					$return_log_array[$new_log_key]['to_address'] = str_replace('=> ', '', $return_log_array[$new_log_key]['to_address'][0]);
				}else{
					unset($return_log_array[$new_log_key]['to_address']);
				}



				$return_log_array[$new_log_key]['raw_log'] = $individual_log;
				
				$new_log_key++;
			}
		}
			return $return_log_array;
	}


	public function groupLogsIntoMessages($individual_logs_with_attrs){
		$return_log_array = array();
		foreach ($individual_logs_with_attrs as $log_key => $log_array) {
			$return_log_array[$log_array['message_id']][] = $log_array;
		}
		return $return_log_array;
	}


	public function setMessageAttrs($message_log_array){
		foreach ($message_log_array as $message_id => $message_array) {
			foreach ($message_array as $log_key => $log_array) {
				if (isset($log_array['type']) == TRUE && $log_array['type'] == 'arrival'){
					$message_log_array[$message_id]['message_attrs']['protocol'] = $log_array['protocol'];
					if (isset($log_array['from_address'])) {
						$message_log_array[$message_id]['message_attrs']['from_address'] = $log_array['from_address'];
					}
					if (isset($log_array['user'])) {
						$message_log_array[$message_id]['message_attrs']['user'] = $log_array['user'];
					}
					if ($log_array['protocol'] !== 'local') {
						$message_log_array[$message_id]['message_attrs']['origin_host'] = $log_array['remote_server'];
					}else{
						$message_log_array[$message_id]['message_attrs']['origin_host'] = "Most likely localhost";
					}
				}
				if (isset($log_array['type']) == TRUE && $log_array['type'] == 'delivery') {
					$message_log_array[$message_id]['message_attrs']['to_address'] = $log_array['to_address'];
					if (isset($log_array['remote_server'])) {
						$message_log_array[$message_id]['message_attrs']['recipient_host'] = $log_array['remote_server'];
					}
				}

			}
		}
		return $message_log_array;
	}
}


