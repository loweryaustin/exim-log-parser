<?php 

class Telpa extends BaseController{

	
public function index()
{
	$this->loadView('form.php', '');
}


	function parse(){
		$raw_logs = $_POST["zeh_logs"];
		if ($raw_logs == NULL) {
			echo "<img src='/assets/images/yunoeximlogs.jpg' width='700px' />";
			echo "<br /><p>Please enter something into the text area before pressing the parse button!";
			die();
		}
		$parseModel = new ParseModel();
		$individual_logs_with_attrs = $parseModel->getIndividualLogAttrs($raw_logs);
		$message_log_array = $parseModel->groupLogsIntoMessages($individual_logs_with_attrs);
		$message_log_array_with_attrs = $parseModel->setMessageAttrs($message_log_array);

		
		$this->loadView('sortedlogs.php', $message_log_array_with_attrs);
	
		if(isset($_POST["debug"])){
			$debug = $_POST["debug"];
		}else{
			$debug = FALSE;
		}

		if($debug){
			echo '<pre>';
			print_r($message_log_array_with_attrs);
			echo '</pre>';
		}

	}
}
