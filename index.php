<?php

//require the general classes
require("classes/loader.php");
require("classes/basecontroller.php");
require("classes/basemodel.php");
require("classes/error.php");
//require the model classes
require("models/parse.php");
//require the controller classes
require("controllers/home.php");
require("controllers/telpa.php");
//create the controller and execute the action
$loader = new Loader($_GET);
$controller = $loader->CreateController();
$controller->ExecuteAction();
