<?php

class Loader {
	private $controller;
	private $action;
	private $urlvalues;
	//store the URL values on object creation
	public function __construct($urlvalues) {
		$this->urlvalues = $urlvalues;
		if ($this->urlvalues['controller'] == "") {
			$this->controller = "home";
		} else {
			$this->controller = $this->urlvalues['controller'];
		}
		if ($this->urlvalues['action'] == "") {
			$this->action = "index";
		} else {
			$this->action = $this->urlvalues['action'];
		}
	}
	//establish the requested controller as an object
	public function CreateController() {
		//does the class exist?
		if (class_exists($this->controller)) {
			$parents = class_parents($this->controller);
			//does the class extend the controller class?
			if (in_array("BaseController",$parents)) {
				//does the class contain the requested method?
				if (method_exists($this->controller,$this->action)) {
					return new $this->controller($this->action,$this->urlvalues);
				} else {
					//bad method error
					return new Error("badUrl",$this->urlvalues, "<title>404 Method Does Not Exist</title>Page not found because:<br />This Method Does Not Exist. You may have entered an invalid url, or there was a misspelling in the code for this page.", 'HTTP/1.0 404 Not Found', '404notfound.jpg');
				}
			} else {
				//bad controller error
				return new Error("badUrl",$this->urlvalues, "<title>404 BaseController Not Extended</title>This Controller Does Not Extend BaseController, check the class definition", 'HTTP/1.0 404 Not Found', '404notfound.jpg');
			}
		} else {
			//bad controller error
			return new Error("badUrl",$this->urlvalues, "<title>404 Controller Does Not Exist</title>Page not found because: <br />This Controller Does Not Exist. You may have entered an invalid url, or the site administrator will need to check to make sure that it is being loaded in the index.php file, and the class defition is located in the /controllers folder.", 'HTTP/1.0 404 Not Found', '404notfound.jpg');
		}
	}
}

