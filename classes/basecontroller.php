<?php

abstract class BaseController {
	protected $urlvalues;
	protected $action;

	public function __construct($action, $urlvalues) {
		$this->action = $action;
		$this->urlvalues = $urlvalues;
	}

	public function ExecuteAction() {
		return $this->{$this->action}();
	}
	
	protected function loadView($view, $data) {
		$viewloc = 'views/'.$view;
			require($viewloc);
	}
}
